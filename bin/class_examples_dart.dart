import 'package:class_examples_dart/layers/libraries/layers_library.dart';
import 'package:class_examples_dart/shapes/abstracts/shapes_abstract.dart';
import 'package:class_examples_dart/shapes/libraries/shapes_library.dart';
import 'package:class_examples_dart/singleton/libraries/singleton_library.dart';

void main(List<String> arguments) {
  // Instantiated as Rectangle
  print('===');
  print('');

  print('Layer 1 calling a Layer 2 Interface, using Polymorphism (Run-time), that is Implemented by Layer 2');
  print('Layer 1 has no visibility of Layer 2, as required by Clean Architecture');
  print('');
  final layer2 = Layer2();
  final layer1 = Layer1(layer2Interface: layer2);
  layer1.sum(list: [1, 2, 3]);

  print('');
  print('===');
  print('');

  print('A CIRCLE instantiated as a CIRCLE');
  print('Although super is not explicitely called, it is implicitly called on Shape() with NO ARGUMENTS');
  print('Shape() then uses CIRCLE\'S name instance variable and CIRCLE\'S area() method rather than it\'s own');
  print('');
  final circle101 = Circle(name: 'CIRCLE', radius: 101);
  Type(shapeAbstract: circle101);

  print('');
  print('---');
  print('');

  print('A CIRCLE instantiated as a SHAPE');
  print('Although super is not explicitely called, it is implicitly called on Shape() with NO ARGUMENTS');
  print('Shape() then uses CIRCLE\'S name instance variable and CIRCLE\'S area() method rather than it\'s own');
  print('');
  ShapeAbstract shape202 = Circle(name: 'CIRCLE', radius: 202);
  Type(shapeAbstract: shape202);

  print('');
  print('---');
  print('');

  print('A RECTANGLE instantiated as a RECTANGLE');
  print('super is now explicitely called on Shape.name(name) with ARGUMENTS');
  print('Shape.name(name) then uses RECTANGLE\'S name instance variable and RECTANGLE\'S area() method rather than it\'s own');
  print('');
  final rectangle303 = Rectangle(name: 'RECTANGLE', length: 303, width: 404);
  Type(shapeAbstract: rectangle303);

  print('');
  print('---');
  print('');

  print('A RECTANGLE instantiated as a SHAPE');
  print('super is now explicitely called on Shape.name(name) with ARGUMENTS');
  print('Shape.name(name) then uses RECTANGLE\'S name instance variable and RECTANGLE\'S area() method rather than it\'s own');
  print('');
  ShapeAbstract shape505 = Rectangle(name: 'RECTANGLE', length: 505, width: 606);
  Type(shapeAbstract: shape505);

  print('');
  print('---');
  print('');

  print('A SQUARE instantiated as a SQUARE');
  print('SQUARE does not implement it\'s own constructor and super is now explicitely called on Rectangle(name, length, width) with ARGUMENTS');
  print('Shape.name(name) and Rectangle(name, length, width) then use SQUARE\'S name instance variable and SQUARE\'S area() method rather than their own');
  print('');
  final square707 = Square(name: 'SQUARE', length: 707);
  Type(shapeAbstract: square707);

  print('');
  print('---');
  print('');

  print('A SQUARE instantiated as a RECTANGLE');
  print('SQUARE does not implement it\'s own constructor and super is now explicitely called on Rectangle(name, length, width) with ARGUMENTS');
  print('Shape.name(name) and Rectangle(name, length, width) then use SQUARE\'S name instance variable and SQUARE\'S area() method rather than their own');
  print('');
  Rectangle rectangle808 = Square(name: 'SQUARE', length: 808);
  Type(shapeAbstract: rectangle808);

  print('');
  print('===');
  print('');

  print('Instantiated 2 Singletons and compared that they are the same instance');
  print('The classes are instantiated as normal');
  print('');
  final singleton1 = Singleton();
  final singleton2 = Singleton();
  if (identical(singleton1, singleton2)) {
    print('PASSED: The instance of singleton1 IS the same as the instance of singleton2');
  } else {
    print('FAILED: The instance of singleton1 is NOT the same as the instance of singleton2');
  }

  print('');
  print('===');
}
