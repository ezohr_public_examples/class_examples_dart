library layers_library.dart;

import 'package:class_examples_dart/layers/interfaces/layers_interface.dart';
import 'package:meta/meta.dart';

@immutable
class Layer1 {
  @protected
  final Layer2Interface layer2Interface;

  Layer1({this.layer2Interface});

  double sum({List<double> list}) {
    final total = layer2Interface.sum(list: list);

    print('LAYER1: The total is $total');

    return total;
  }
}

@immutable
class Layer2 implements Layer2Interface {
  @override
  double sum({List<double> list}) {
    // TODO: Can this be made immutable
    var total = 0.0;

    list.forEach((element) => total += element);

    print('LAYER2: The total is $total');

    return total;
  }
}
