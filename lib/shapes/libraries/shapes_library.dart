library shapes_library.dart;

// TODO: Shapes Unit Tests using the Test Cases Framework
// TODO: Rework so all Classes are immutable

import 'dart:math';

import 'package:class_examples_dart/shapes/abstracts/shapes_abstract.dart';
import 'package:meta/meta.dart';

class Circle extends ShapeAbstract {
  @protected
  final double radius;
  @override
  @protected
  String name = 'NO NAME';

  Circle({@required this.name, @required this.radius}) {
    print('CIRCLE: I\'m a $name with a RADIUS of $radius');

    area();
  }

  @override
  void area() {
    final area = pi * radius * radius;

    print('CIRCLE: My AREA is $area');
    print('');
  }
}

class Rectangle extends ShapeAbstract {
  @protected
  final double length, width;
  @override
  @protected
  String name = 'NO NAME';

  Rectangle({@required this.name, @required this.length, @required this.width}) : super.name(name: 'SHAPE OF RECTANGLE') {
    print('RECTANGLE: I\'m a $name with a LENGTH of $length and a WIDTH of $width');

    area();
  }

  @override
  void area() {
    final area = length * width;

    print('RECTANGLE: My AREA is $area');
    print('');
  }
}

class Square extends Rectangle {
  @override
  @protected
  final double length;
  @override
  @protected
  String name = 'NO NAME';

  Square({@required this.name, @required this.length}) : super(name: 'RECTANGLE OF SQUARE', length: length, width: length);
  // {
  //   print('SQUARE: I\'m a $name with a LENGTH of $length');

  //   area();
  // }

  @override
  void area() {
    final area = length * length;

    print('SQUARE: My AREA is $area');
    print('');
  }
}

@immutable
class Type {
  Type({@required ShapeAbstract shapeAbstract}) {
    print('TYPE: I\'m passed in as a SHAPE, but am of type $shapeAbstract');
  }
}
