import 'package:class_examples_dart/shapes/interfaces/shapes_interface.dart';
import 'package:meta/meta.dart';

// TODO: Rework so all Classes are immutable

abstract class ShapeAbstract implements AreaInterface {
  @protected
  String name = 'NO NAME';

  ShapeAbstract() {
    print('SHAPE: I\'m a SHAPE called $name');

    area();
  }

  ShapeAbstract.name({@required this.name}) {
    print('SHAPE: I\'m a SHAPE called $name');

    area();
  }

  @override
  void area() {
    print('SHAPE: My AREA is UNKNOWN');
    print('');
  }
}
