library singleton_library.dart;

import 'package:meta/meta.dart';

// Singleton Class

@immutable
class Singleton {
  // Singleton Pattern

  @protected
  static final Singleton _singleton = Singleton._();

  factory Singleton() {
    return _singleton;
  }

  @protected
  Singleton._();
}
