# Class Examples (Dart)

## Layers

### Clean Architecture Example And Documentation

> Documentation
>
> Creating the Interface
>
> Implementing the Interface
>
> Using Polymorphism to call the Interface
>
> Layers Unit Test using the Test Cases Framework

## Shapes

### Class Inheritance, Polymorphism And Interfaces Examples

> Upcasting of Subclass types to their Super-class types
>
> Implicit calling of the Super-class' Unnamed Zero Argument Constructor
>
> Implicit use of the Subclass' overridden Instance Variables and Methods by the Super-class
>
> Explicit calling of the Super-class' Named Constructor
>
> Implementing an Interface in the Super-class
>
> Behaviour in implementing a Sub-subclass
>
> Behaviour in not implementing a Subclass Constructor

## Singleton

### Singleton Example

> Creating a Singleton
>
> Singleton Unit Test using the Test Cases Framework
