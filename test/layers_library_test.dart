import 'package:class_examples_dart/layers/libraries/layers_library.dart';
import 'package:general_libraries_examples_dart/test_cases/abstracts/test_cases_abstract.dart';
import 'package:meta/meta.dart';
import 'package:test/test.dart';

@immutable
class TestCaseStructure extends TestCaseStructureAbstract {
  final List<double> list;

  TestCaseStructure({@required positive, @required description, @required matcher, @required this.list}) : super(positive: positive, description: description, matcher: matcher);
}

@immutable
class TestCases extends TestCasesAbstract {
  @protected
  @override
  final String testGroup = 'Layers';

  @override
  @protected
  List<TestCaseStructure> definedTestCases() {
    // TODO: Can this be made immutable
    var testCases = <TestCaseStructure>[];

    testCases.add(TestCaseStructure(
      positive: true,
      description: '[1, 2, 3]',
      matcher: 6,
      list: [1, 2, 3],
    ));

    return testCases;
  }
}

@immutable
class ClassTests extends ClassTestsAbstract {
  ClassTests({@required String testGroup, @required List<TestCaseStructure> testCases}) : super(testGroup: testGroup, testCases: testCases);

  @override
  @protected
  void testTestCase({@required TestCaseStructureAbstract testCase}) {
    final testCaseDowncast = testCase as TestCaseStructure;

    final testType = testCaseDowncast.positive ? 'Positive:' : 'Negative:';

    test('$testType ${testCaseDowncast.description}', () async {
      // Arrange
      final testClassLayer2 = Layer2();
      final testClassLayer1 = Layer1(layer2Interface: testClassLayer2);

      // Act
      final actual = testClassLayer1.sum(list: testCaseDowncast.list);

      // Assert
      expect(actual, testCaseDowncast.matcher);
    });
  }
}

void main() {
  final testCases = TestCases();

  ClassTests(testGroup: testCases.getTestGroup, testCases: testCases.getTestCases);
}
