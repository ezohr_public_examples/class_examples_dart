import 'package:class_examples_dart/singleton/libraries/singleton_library.dart';
import 'package:general_libraries_examples_dart/test_cases/abstracts/test_cases_abstract.dart';
import 'package:meta/meta.dart';
import 'package:test/test.dart';

@immutable
class TestCaseStructure extends TestCaseStructureAbstract {
  TestCaseStructure({@required positive, @required description, @required matcher}) : super(positive: positive, description: description, matcher: matcher);
}

@immutable
class TestCases extends TestCasesAbstract {
  @protected
  @override
  final String testGroup = 'Singleton';

  @override
  @protected
  List<TestCaseStructure> definedTestCases() {
    // TODO: Can this be made immutable
    var testCases = <TestCaseStructure>[];

    final matcher = Singleton();

    testCases.add(TestCaseStructure(
      positive: true,
      description: '',
      matcher: matcher,
    ));

    return testCases;
  }
}

@immutable
class ClassTests extends ClassTestsAbstract {
  ClassTests({@required String testGroup, @required List<TestCaseStructure> testCases}) : super(testGroup: testGroup, testCases: testCases);

  @override
  @protected
  void testTestCase({@required TestCaseStructureAbstract testCase}) {
    final testCaseDowncast = testCase as TestCaseStructure;

    final testType = testCaseDowncast.positive ? 'Positive:' : 'Negative:';

    test('$testType ${testCaseDowncast.description}', () async {
      // Arrange

      // Act
      final actual = Singleton();

      // Assert
      expect(actual, testCaseDowncast.matcher);
    });
  }
}

void main() {
  final testCases = TestCases();

  ClassTests(testGroup: testCases.getTestGroup, testCases: testCases.getTestCases);
}
